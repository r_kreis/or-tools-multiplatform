/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package cc.ilo.ortools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class OrToolsMultiplatform {
    private static boolean initialized = false;
    public static synchronized  void init(){
        if(!initialized) {
            String version = getVersion();
            if(version == null || version.trim().isEmpty()){
                throw new RuntimeException("Cannot determine OR-Tools  Version");
            }
            String arch = System.getProperty("os.arch");
            String os = System.getProperty("os.name");
            String libraryPath;
            String extension;
            if (os.contains("Windows")) {
                extension = "dll";
                if (arch.contains("64")) {
                    libraryPath = "/lib/ortools-"+version+"-win64.dll";
                } else {
                    libraryPath = "/lib/ortools-"+version+"-win32.dll";
                }
            } else if (os.contains("Mac OS X")) {
                extension = "jnilib";
                libraryPath = "/lib/ortools-"+version+"-osx.jnilib";
            } else { //Linux
                extension = "so";
                if (arch.contains("64")) {
                    libraryPath = "/lib/ortools-"+version+"-linux32.so";
                } else {
                    libraryPath = "/lib/ortools-"+version+"-linux64.so";
                }
            }
            InputStream source = OrToolsMultiplatform.class.getResourceAsStream(libraryPath);
            File tempFile = null;
            try {
                tempFile = File.createTempFile("lib-or-tools-temp", "." + extension);

                FileOutputStream dest = new FileOutputStream(tempFile);
                try {
                    copy(source, dest);
                } finally {
                    dest.close();
                    source.close();
                }
                System.load(tempFile.getAbsolutePath());
                tempFile.deleteOnExit();
                initialized = true;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void copy(InputStream input, FileOutputStream output) throws IOException {
        final byte[] buffer = new byte[2000];
        int n;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
    }

    //Source:
    // http://stackoverflow.com/questions/2712970/get-maven-artifact-version-at-runtime
    private static String getVersion(){
        String version = null;

        // try to load from maven properties first
        try {
            Properties p = new Properties();
            InputStream is = OrToolsMultiplatform.class.getResourceAsStream("/META-INF/maven/cc.ilo.ortools/or-tools-multiplatform/pom.properties");
            if (is != null) {
                p.load(is);
                version = p.getProperty("version", "");
            }
        } catch (Exception e) {
            // ignore
        }

        // fallback to using Java API
        if (version == null) {
            Package aPackage = OrToolsMultiplatform.class.getPackage();
            if (aPackage != null) {
                version = aPackage.getImplementationVersion();
                if (version == null) {
                    version = aPackage.getSpecificationVersion();
                }
            }
        }

        if (version == null) {
            // we could not compute the version so use a blank
            version = "";
        }

        return version;
    }
}
